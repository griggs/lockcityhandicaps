﻿using LockCityHandicaps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockCityHandicaps
{
    /// <summary>
    /// Interaction logic for Players.xaml
    /// </summary>
    public partial class Players : Page
    {
        private int TeamId;
        readonly Repo repo = new Repo();
        public Players(int _teamId)
        {
            InitializeComponent();

            TeamId = _teamId;

            PlayerGrid.ItemsSource = repo.GetPlayersByTeam(TeamId);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var players = (IEnumerable<Player>)PlayerGrid.ItemsSource;

            foreach (var item in players)
            {
                item.TeamId = TeamId;

                if (item.Id == 0)
                {
                    repo.CreatePlayer(item);
                }
                else
                {
                    repo.UpdatePlayer(item);
                }
            }

            PlayerGrid.ItemsSource = repo.GetPlayersByTeam(TeamId);

        }

        private void DeletePlayer(object sender, RoutedEventArgs e)
        {
            repo.DeletePlayer((Player)PlayerGrid.SelectedItem);
            PlayerGrid.ItemsSource = repo.GetPlayersByTeam(TeamId);
        }
    }
}
