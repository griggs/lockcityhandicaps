﻿using LockCityHandicaps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockCityHandicaps
{
    public partial class Print : Page
    {
        readonly Repo repo = new Repo();
        public Print()
        {
            InitializeComponent();

        }

        private void Handicaps_Click(object sender, RoutedEventArgs e)
        {
            var pdfC = new PDFCreator();
            pdfC.CreateHandicapPDF(txtYear.Text, txtWeek.Text);
        }


    }
}
