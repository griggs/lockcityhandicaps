﻿using Dapper;
using LockCityHandicaps.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LockCityHandicaps
{
    public class Repo
    {

        public const int LatestDatabaseVersion = 2;

        public static string DbFile
        {
            get { return Environment.CurrentDirectory + $@"\{ConfigurationManager.AppSettings["dbFile"]}"; }
        }

        public static SQLiteConnection SimpleDbConnection()
        {
            var conn = new SQLiteConnection("Data Source=" + DbFile);
            conn.Open();
            return conn;
        }

        public static int DBVersion
        {
            get { using (var conn = SimpleDbConnection()){return conn.QueryFirst<int>("PRAGMA user_version;");}}
            set
            {
                using (var conn = SimpleDbConnection())
                {
                    conn.Execute($"PRAGMA user_version = {value};");
                }
            }
        }

        internal void CreateScore(Score item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"INSERT INTO Score(Date,PlayerId,RawScore,ESCScore) VALUES('{item.Date}', {item.PlayerId}, {item.RawScore}, {item.ESCScore});");

            }
        }

        internal IEnumerable<Score> GetScores()
        {
            using (var conn = SimpleDbConnection())
            {
                return conn.Query<Score>($"SELECT S.*, P.Name as PlayerName FROM Score S JOIN Player P ON P.Id = S.PlayerId;");
            }
        }

        internal void DeleteScore(Score item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"DELETE FROM Score WHERE Id = {item.Id};");
            }
        }

        internal IEnumerable<Score> GetPlayerScores(int PlayerId)
        {
            using (var conn = SimpleDbConnection())
            {
                return conn.Query<Score>($"SELECT S.* FROM Score S Where S.PlayerId = {PlayerId};");
            }
        }

        internal IEnumerable<Player> GetPlayers()
        {
            using (var conn = SimpleDbConnection())
            {
                return conn.Query<Player>($"SELECT P.*, T.Name as TeamName FROM Player P JOIN Team T on T.Id = P.TeamId;");
            }
        }

        internal void CreatePlayer(Player item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"INSERT INTO Player(Name,TeamId, PlaysWhites) VALUES('{item.Name}', {item.TeamId}, {item.PlaysWhites});");
            }
        }

        internal void UpdatePlayer(Player item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"UPDATE Player SET Name = '{item.Name}', PlaysWhites = {item.PlaysWhites} WHERE Id = {item.Id};");
            }
        }

        internal void DeletePlayer(Player item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"DELETE FROM Player WHERE Id = {item.Id};");
            }
        }

        internal IEnumerable<Player> GetPlayersByTeam(int teamId)
        {
            using (var conn = SimpleDbConnection())
            {
                return conn.Query<Player>($"SELECT * FROM Player WHERE TeamId = {teamId};");
            }
        }

        internal IEnumerable<Team> GetTeams()
        {
            using (var conn = SimpleDbConnection())
            {
                return conn.Query<Team>($"SELECT * FROM Team;");
            }
        }

        internal void CreateTeam(Team item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"INSERT INTO Team(Name) VALUES('{item.Name}');");
            }
        }

        internal void UpdateTeam(Team item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"UPDATE Team SET Name = '{item.Name}' WHERE Id = {item.Id};");
            }
        }

        internal void DeleteTeam(Team item)
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute($"DELETE FROM Team WHERE Id = {item.Id};");
            }
        }


        public static void MigrateDatabase()
        {
            while(DBVersion != LatestDatabaseVersion)
            {
                switch(DBVersion)
                {
                    case 0:
                        CreateDatabase();
                        DBVersion = 1;
                        break;
                    case 1:
                        AddPlaysFromWhites();
                        DBVersion = 2;
                        break;
                }

            }

        }

        private static void AddPlaysFromWhites()
        {
            using (var conn = SimpleDbConnection())
            {
                conn.Execute(
                    @"alter table Player add column PlaysWhites integer"
                );
            }
        }

        public static void CreateDatabase()
        {
            using (var conn = SimpleDbConnection())
            {
                
                conn.Execute(
                    @"create table Team(
                        Id integer primary key AUTOINCREMENT,
                        Name varchar(100) not null
                    )"
                );

                conn.Execute(
                    @"create table Player(
                        Id integer primary key AUTOINCREMENT,
                        Name varchar(100) not null,
                        TeamId integer not null
                    )"
                );

                conn.Execute(
                    @"create table Score(
                        Id integer primary key AUTOINCREMENT,
                        Date TEXT not null,
                        PlayerId integer not null,
                        RawScore integer not null,
                        ESCScore integer not null
                    )"
                );
            }
        }
    }
}
