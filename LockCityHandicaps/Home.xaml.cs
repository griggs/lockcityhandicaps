﻿using LockCityHandicaps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockCityHandicaps
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        readonly Repo repo = new Repo();
        public Home()
        {
            InitializeComponent();

            //Get all players and scores
            var players = repo.GetPlayers().ToList();
            var calculator = new HandicapCalculator();
            calculator.Process(players);
            
            //set grid.
            grdHandicaps.ItemsSource = players.OrderBy(x => x.TeamName).ThenBy(x => x.Name);
        }

       
    }
}
