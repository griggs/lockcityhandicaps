﻿using LockCityHandicaps.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockCityHandicaps
{
    /// <summary>
    /// Interaction logic for Teams.xaml
    /// </summary>
    public partial class Teams : Page
    {
        readonly Repo repo = new Repo();
        public Teams()
        {
            InitializeComponent();

            TeamGrid.ItemsSource = repo.GetTeams();
            
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var teams = (IEnumerable<Team>)TeamGrid.ItemsSource;

            foreach(var team in teams)
            {
                if(team.Id == 0)
                {
                    repo.CreateTeam(team);
                }
                else
                {
                    repo.UpdateTeam(team);
                }
            }

            TeamGrid.ItemsSource = repo.GetTeams();

        }

        private void DeleteTeam_Click(object sender, RoutedEventArgs e)
        {
            repo.DeleteTeam((Team)TeamGrid.SelectedItem);
            TeamGrid.ItemsSource = repo.GetTeams();
        }

        private void Players_Click(object sender, RoutedEventArgs e)
        {
            
            NavigationService.Navigate(new Players(((Team)TeamGrid.SelectedItem).Id));
        }
    }
}
