﻿using LockCityHandicaps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockCityHandicaps
{
    /// <summary>
    /// Interaction logic for Scores.xaml
    /// </summary>
    public partial class Scores : Page
    {
        readonly Repo repo = new Repo();
        public Scores()
        {
            InitializeComponent();

            cboPlayer.ItemsSource = repo.GetPlayers().OrderBy(x => x.Name);
            cboPlayer.SelectedIndex = 0;

            ScoreGrid.ItemsSource = repo.GetScores();
            
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            int.TryParse(txtRawScore.Text.Trim(), out var rawScore);
            int.TryParse(txtESCScore.Text.Trim(), out var escScore);
            var score = new Score() { Date = dtpDate.SelectedDate.Value, PlayerId = (int)cboPlayer.SelectedValue, RawScore = rawScore, ESCScore = escScore};
            repo.CreateScore(score);

            ScoreGrid.ItemsSource = repo.GetScores();

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            repo.DeleteScore((Score)ScoreGrid.SelectedItem);
            ScoreGrid.ItemsSource = repo.GetScores();
        }
    }
}
