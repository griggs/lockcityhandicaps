﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockCityHandicaps
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            if(Repo.DBVersion != Repo.LatestDatabaseVersion)
            {
                Repo.MigrateDatabase();
            }

            
        }

        private void ShowHomePage(object sender, RoutedEventArgs e)
        {
            PageContent.NavigationService.Navigate(new Home());
        }

        private void ShowTeamPage(object sender, RoutedEventArgs e)
        {
            PageContent.NavigationService.Navigate(new Teams());
        }

        private void ShowScorePage(object sender, RoutedEventArgs e)
        {
            PageContent.NavigationService.Navigate(new Scores());
        }

        private void PrintHandicaps(object sender, RoutedEventArgs e)
        {
            PageContent.NavigationService.Navigate(new Print());           
        }
    }
}
