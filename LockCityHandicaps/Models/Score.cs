﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LockCityHandicaps.Models
{
    public class Score
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int PlayerId { get; set; }
        public int RawScore { get; set; }
        public int ESCScore { get; set; }

        public virtual string PlayerName { get; set; }
    }
}
