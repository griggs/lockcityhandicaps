﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LockCityHandicaps.Models
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool PlaysWhites { get; set; }
        public int TeamId { get; set; }

        public virtual List<Score> Scores { get; set; }
        public virtual int? Handicap { get; set; }
        public virtual string TeamName { get; set; }
    }
}
