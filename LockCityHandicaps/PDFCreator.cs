﻿using iText.IO.Font.Constants;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using System.IO;
using System.Linq;

namespace LockCityHandicaps
{
    public class PDFCreator
    {
        public void CreateHandicapPDF(string year, string week)
        {
            var repo = new Repo();
            var players = repo.GetPlayers().ToList();
            var calculator = new HandicapCalculator();
            calculator.Process(players);

            players = players.OrderBy(x => x.TeamName).ThenBy(x => x.Name).ToList();

            if(!Directory.Exists("Handicaps"))
            {
                Directory.CreateDirectory("Handicaps");
            }

            // Create an instance of the document class which represents the PDF document itself.  
            var writer = new PdfWriter($@"Handicaps\{year}_{week}_Handicaps.pdf");

            //Initialize PDF document

            var pdf = new PdfDocument(writer);
            // Initialize document

            Document document = new Document(pdf);

            var font = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);
            var bold = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
            Table table = new Table(4);
            table.AddHeaderCell(new Cell().Add(new Paragraph("Team").SetFont(bold)));
            table.AddHeaderCell(new Cell().Add(new Paragraph("Name").SetFont(bold)));
            table.AddHeaderCell(new Cell().Add(new Paragraph("Plays Whites").SetFont(bold)));
            table.AddHeaderCell(new Cell().Add(new Paragraph("Handicap").SetFont(bold)));
            //Add paragraph to the document
            foreach (var player in players)
            {
                table.AddCell(new Cell().Add(new Paragraph(player.TeamName).SetFont(font)));
                table.AddCell(new Cell().Add(new Paragraph(player.Name).SetFont(font)));
                table.AddCell(new Cell().Add(new Paragraph(player.PlaysWhites ? "Y" : "").SetFont(font)).SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER));
                table.AddCell(new Cell().Add(new Paragraph(player.Handicap.ToString()).SetFont(font)).SetHorizontalAlignment(iText.Layout.Properties.HorizontalAlignment.CENTER));                
            }

            document.Add(table);
            //Close document

            document.Close();
        }
    }
}
