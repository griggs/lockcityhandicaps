﻿using LockCityHandicaps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LockCityHandicaps
{
    public class HandicapCalculator
    {
        readonly Repo repo = new Repo();
        public void Process(List<Player> players)
        {
            players.ForEach(x => x.Scores = repo.GetPlayerScores(x.Id).ToList());

            //calculate handicaps
            players.ForEach(x => x.Handicap = CalculateHandicap(x));
        }

        public int? CalculateHandicap(Player player)
        {

            if (player.Scores.Count == 0)
            {
                return null;
            }

            List<int> diffs;

            //Calculate all differentials
            if (player.PlaysWhites)
            {
                diffs = player.Scores.Select(x => CalculateWhiteDiff(x.ESCScore)).ToList();
            }
            else
            {
                diffs = player.Scores.Select(x => CalculateBlueDiff(x.ESCScore)).ToList();
            }

            //Get Diffs to use
            var diffsToUse = CalculateDiffsToUse(diffs.Count);

            //Average together and multiply by 96%
            var avg = diffs.OrderBy(x => x).Take(diffsToUse).Average(x => x);
            var final = (int)Math.Round(avg * .96, MidpointRounding.AwayFromZero);
            return final > 18 ? 18 : final;
        }

        private static int CalculateDiffsToUse(int count)
        {
            switch (count)
            {
                case 0:
                    return 0;
                case 1:
                case 2:
                case 3:
                    return 1;
                case 4:
                case 5:
                    return 2;
                case 6:
                case 7:
                case 8:
                    return 3;
                case 9:
                case 10:
                case 11:
                case 12:
                    return 4;
                case 13:
                case 14:
                    return 5;
                case 15:
                case 16:
                    return 6;
                case 17:
                    return 7;
                case 18:
                    return 8;
                case 19:
                    return 9;
                case 20:
                    return 10;
                default:
                    return 10;
            }
        }

        //Differential Formula : (ESC Score – Course Rating) * 113 / Slope Rating
        private int CalculateBlueDiff(int score)
        {
            //Rating 35, 113
            return (score - 35) * 113 / 113;
        }

        private int CalculateWhiteDiff(int score)
        {
            //Rating 34, 110
            return (score - 34) * 113 / 110;
        }
    }
}
